package com.ardilla.shinemoney

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.os.bundleOf
import androidx.core.text.isDigitsOnly
import androidx.fragment.app.add
import androidx.fragment.app.commit
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.properties.Delegates

class MainActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener {
    // The price is in euro
    var priceForKWh = 0.10
    var monthlyPrice by Delegates.notNull<Double>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        spinner.onItemSelectedListener = this
        btnCalculate.setOnClickListener {
            handleCalculatePrice()
        }
        btnInformation.setOnClickListener {
            supportFragmentManager.commit {
                setReorderingAllowed(true)
                add<InformationFragment>(R.id.rlMain)
            }
        }

        val categories: MutableList<String> = ArrayList()
        categories.add("Bulgaria")
        categories.add("England")
        categories.add("Germany")
        categories.add("America")

        val dataAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, categories)
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner.adapter = dataAdapter
    }

    override fun onBackPressed() {
        if (supportFragmentManager.fragments.size > 0) {
            supportFragmentManager.fragments.forEach { fragment ->
                supportFragmentManager.beginTransaction().remove(fragment).commit()
            }
        } else {
            super.onBackPressed()
        }
    }

    private fun handleCalculatePrice() {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(rlMain!!.windowToken, 0)
        if (tvPower.text.isNullOrEmpty() || tvTime.text.isNullOrEmpty() || tvNumber.text.isNullOrEmpty()) {
            Toast.makeText(this, "Fill all the field", Toast.LENGTH_LONG).show()
            return
        }
        if (!tvPower.text.isDigitsOnly() || !tvTime.text.isDigitsOnly() || !tvNumber.text.isDigitsOnly()) {
            Toast.makeText(this, "Fields must contain only numbers", Toast.LENGTH_LONG).show()
            return
        }
        monthlyPrice = ((tvPower.text.toString().toDouble() / 1000) * (tvTime.text.toString().toDouble() * 30) * priceForKWh) * tvNumber.text.toString().toDouble()
        monthlyPrice = Math.round(monthlyPrice * 100.0) / 100.0
        supportFragmentManager.commit {
            setReorderingAllowed(true)
            add<CalculateFragment>(R.id.rlMain)
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        when (parent?.getItemAtPosition(position).toString()) {
            "Bulgaria" -> priceForKWh = 0.10
            "England" -> priceForKWh = 0.22
            "Germany" -> priceForKWh = 0.30
            "America" -> priceForKWh = 0.11
        }
    }
}