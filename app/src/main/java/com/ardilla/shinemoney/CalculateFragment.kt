package com.ardilla.shinemoney

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_results.*

class CalculateFragment: Fragment(R.layout.fragment_results){
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        tvResults.post {
            tvResults.text = (activity as MainActivity).monthlyPrice.toString() + "€"
        }
    }
}